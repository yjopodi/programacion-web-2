const mongoose = require('mongoose');

var schema = new mongoose.Schema({
    name : {
        type : String,
        required: true
    },
    code : {
        type: String,
        required: true
    },
    career : {
        type: String,
        required: true
    },
    credits : {
        type: Number 
    }
})

const Coursedb = mongoose.model('course', schema);

module.exports = Coursedb;