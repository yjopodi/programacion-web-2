const Tarea = require("../modelos/tarea_Modelo");
const tareaPost = (req, res) => {
    var tarea = new Tarea();
  
    tarea.titulo = req.body.titulo;
    tarea.detalle = req.body.detalle;
  
    if (tarea.titulo && tarea.detalle) {
      tarea.save(function (err) {
        if (err) {
          res.status(422);
          console.log('error al guardar la tarea', err)
          res.json({
            error: 'se dio un error al intentar agregar la tarea'
          });
        }
        res.status(201);//CREATED
        res.header({
          'location': `http://localhost:3000/api/tareas/?id=${tarea.id}`
        });
        res.json(tarea);
      });
    } else {
      res.status(422);
      console.log('error al guardar la tarea')
      res.json({
        error: 'datos proporcionados invalidos'
      });
    }
  };
  const tareaGet = (req, res) => {
    // si se requiere una tarea específica
    if (req.query && req.query.id) {
      Tarea.findById(req.query.id, function (err, tarea) {
        if (err) {
          res.status(404);
          console.log('error while queryting the task', err)
          res.json({ error: "Task doesnt exist" })
        }
        res.json(tarea);
      });
    } else {
      // obtener todos las tareas
      Tarea.find(function (err, tareas) {
        if (err) {
          res.status(422);
          res.json({ "error": err });
        }
        res.json(tareas);
      });
  
    }
  };
  const tareaPatch = (req, res) => {
    // get task by id
    if (req.query && req.query.id) {
      Tarea.findById(req.query.id, function (err, task) {
        if (err) {
          res.status(404);
          console.log('error while queryting the task', err)
          res.json({ error: "Task doesnt exist" })
        }
  
        // update the task object (patch)
        tarea.titulo = req.body.titulo ? req.body.titulo : task.titulo;
        tarea.detalle = req.body.detalle ? req.body.detalle : task.detalle;
        // update the task object (put)
        // task.title = req.body.title
        // task.detail = req.body.detail
  
        tarea.save(function (err) {
          if (err) {
            res.status(422);
            console.log('error while saving the task', err)
            res.json({
              error: 'There was an error saving the task'
            });
          }
          res.status(200); // OK
          res.json(tarea);
        });
      });
    } else {
      res.status(404);
      res.json({ error: "Task doesnt exist" })
    }
  };
  
  module.exports = {
    tareaGet,
    tareaPost,
    tareaPatch
  }