const express = require('express');
const cors = require("cors");
const app = express();
'use strict';


app.use(cors({
    domains: '*',
    methods: "*"
  }));
const path = require('path');

  app.listen(3000, () => console.log(`Example app listening on port 3000!`));
